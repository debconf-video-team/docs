.. _loops:

Loops
=====

.. NOTE::
   We have not been able to loop anything else than MPEG-2 Transport stream
   (.ts) in ``gstreamer`` yet.

Our recording and streaming setup uses several video loops:

 - When not streaming talks, we show a loop with the logo of the conference, as
   well as the logos of the conference sponsors if applicable. This is called
   the *sponsor loop* (``loop.ts``).

 - While streaming, in some composite modes (side-by-side equal and side-by-side
   preview), some space is available to show a background. Voctomix allows us to
   display arbitrary video there, and we usually generate a video loop from one
   or two static images. This is called the *background loop* (``bgloop.ts``).

Both loops can be generated from the same sets of commands, described below.

Creating a loop from a single static image
------------------------------------------

If you only have a single image, ``ffmpeg`` can create a video loop for you::

  ffmpeg -loop 1 -i loop.png \
  -c:v mpeg2video -pix_fmt:v yuv420p -qscale:v 2 -qmin:v 2 -qmax:v 7 \
  -keyint_min 0 -bf 0 -g 0 -maxrate:0 90M -t 10 loop.ts

This will create a 10 second loop file.

It can also do effects, like this which fades between colour and greyscale::

  ffmpeg -loop 1 -i loop.png \
  -filter_complex "[0:v] scale=1280x720 [scaled]; [scaled] hue='s=sin(.5*PI*t)+1' [vid]" \
  -map "[vid]" -c:v mpeg2video -pix_fmt:v yuv420p -qscale:v 2 -qmin:v 2 -qmax:v 7 \
  -keyint_min 0 -bf 0 -g 0 -maxrate:0 90M -t 10 loop.ts

Creating a loop with a fade-in from multiple images
---------------------------------------------------

You can also use ``ffmpeg`` to create a loop out of multiple images with a
fade-in between them.

Start by building a series of slides as separate PNG files. Make sure they all
use the correct resolution (1280x720 in our case). To get a proper fade-in, add
"blank" slides in between.

Slides should be named in a sequential order, starting from ``01.png``. If you
need some slides to be longer, you can double them.

Once your slides are ready, you can `run this script`_ to create the loop::

   $ ./loop_transition.sh path_to_slides_dir

.. _`run this script`: https://salsa.debian.org/debconf-video-team/ansible/blob/main/roles/voctomix/files/loop_transition.sh

Safe zones for the background loop
----------------------------------

The background loop is only partially visible most of the time. When a source is
displayed at full screen, the background loop is hidden. However, when either of
the side-by-side modes are selected, the space not used in by the main video
sources shows the background loop. This means that special care should be taken
to ensure that elements in the background loop are placed correctly. The visible
sections are commonly known as safe zones.

The following template shows which areas of the background loop are visible in
various modes. A background checkerboard is provided for size reference.

 - The upper-right and lower-left areas of the loop are always visible.
 - Additionally, in Side-by-Side Equals, the top and bottom of the loop are visible.
 - In Side-by-Side Preview, a larger portion of the right-hand side of the loop
   is visible.

.. image:: _static/composite_modes.svg
   :alt: Voctomix compositing modes and zones available for background graphics
   :target: _static/composite_modes.svg

You can download the SVG to use as a template, playing with the layer visibility
settings to check whether the background has a sensible rendering or not.

As a background, we suggest a solid color or a light gradient (matching the
conference theme), and adding a logo and/or some text (e.g. dates) in the
"Always visible" zones only. If that doesn't suit you, you can try having
redundant content in the "Only visible in Side-by-Side Equals" and "Only
visible in Side-by-Side Preview" zones, but this usually looks worse than just
static content.

.. image:: _static/background_example.png
   :alt: Example background image
   :target: _static/background_example.png
