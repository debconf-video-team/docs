Camera Troubleshooting
======================

Too much noise / grainy

  - Lower the gain
  - Lower iris setting if pictures gets too dark.

Picture too dark

  - Increase the gain, (watch out for noise.)
  - Lower the iris settings (watch out for focus problems)
  - Decrease shutter settings to have a longer exposure.

Picture too bright

  - Increase iris settings.
  - Decrease gain.
  - Increase shutter settings.

Out of focus

  - Check if focus is set to manual (either mountains or hand with an 'F' in
    screen) and go to auto if desired.
  - If focus is set to auto and blurs or focusses the wrong thing, switch to
    manual focus: - press AF/MF (make sure the switch is set to focus, not to
    zoom)
