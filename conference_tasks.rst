Conference Tasks
================

There are things that need to be done every day during a conference.

Morning
--------

  - Replace batteries
  - Turn on projector
  - Check cameras are running

Before lunch break
------------------

  - Sync videos

Evening
-------

  - Sync videos
  - Turn off microphones
  - Turn off projector
