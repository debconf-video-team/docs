Getting started
===============

.. _index:

.. toctree::
   :hidden:
   :maxdepth: 2

   advice_for_presenters.rst
   advice_for_recording.rst
   working_with_videoteam.rst
   hardware.rst
   ansible.rst
   streaming.rst
   review.rst
   venue_checklist.rst
   room_setup.rst
   camera_setup.rst
   opsis.rst
   storage.rst
   loops.rst
   video_archive.rst
   volunteer_roles.rst
   online_volunteer_roles.rst
   conference_tasks.rst
   troubleshooting.rst
   contact.rst

Welcome to the DebConf video team documentation. The goal of this
website is to give you the necessary tools to replicate our setup as
closely as possible. It also includes some extra information about tips and
tricks we find useful when dealing with equipment, volunteers and speakers.

If you are part of the Debian community and plan to organise an event you
intend to record, please consult this guide detailing how we can :ref:`work
together <working with videoteam>`. Maybe a member of our team can attend it and
help you with your setup. If not, we can certainly give you some advice or
lend you some hardware.

Our documentation is divided in a way that makes it easy for you to replicate
what we do. First, take a look at the :ref:`hardware` we use to see what kind of
gear you will need. Once that is done you should check out the article on how
to use our :ref:`ansible` playbooks to setup the different machines you'll be using.

The external setup for :ref:`streaming` and :ref:`video
reviewing<review>` described here can be a lot of trouble to set.
If you don't want to do this yourself and you are organising something
related to Debian, send us a mail on our `mailing list`_ and we'll see
if you can use our instances.

.. _mailing list: https://lists.debian.org/debconf-video/

Our general setup
-----------------

If you are not familiar with our work, our goal is to record various conferences
related to Debian during the year.

We do so on a mix of hardware we own and hardware we rent. You can learn more
about the general physical :ref:`room setup` we use.. The output from
the cameras and the microphone is sent to our live-mixing computer running
`voctomix`_. The footage that is mixed live is then saved on the local
computer's disk and live streamed online.

Once the talk is over, the local recording is sent to a central server to be
reviewed and further edited by a member of the team. Once that is done, we
transcode the raw files to an acceptable filesize and upload it to our
:ref:`archive`.

.. _voctomix: https://github.com/voc/voctomix
