.. _venue checklist:

Venue checklist
===============

When organising a DebConf, there are a number of things the local team needs to
investigate so that the video team knows what to expect when they arrive to
set-up. Most of these are physical constraints of the rooms hired, which makes
pictures of the rooms and existing equipment useful.

The biggest question is how large the rooms are. This affects the :ref:`room
setup`. The positioning and number of power and network sockets as well as the
speed of the network sockets are also crucial. If there are floor plans of the
rooms, they make planning of the setup much easier.

To stream the conference, we need at least a network uplink with a 5mbps
dedicated connection. Although not a requirement, having at least a single
static public IP that we can NAT behind is appreciated. If we can get a range
of IPs this makes things easier. Our setup requires gigabit connectivity
between all our machines.

Another consideration is how cables can be run between the front and the back of
the talk rooms. This includes any local safety laws that may prevent us from
running cables in specific areas or tape them down to the floor. If there is a
false ceiling that cables can be hung from, this is often the easiest solution.

Physical Space
--------------

In each room covered by the video team, there are typically 2 video
cameras: One facing the presentation, from the back of the room, and one
facing the audience from the side of the stage.

There's a table to be set up in the back (or side) of the room for 3
people to sit at, typically next to the rear camera:

#. Director (live video mixer) using a desktop PC.
#. Audio Mixer (for the videos and the room PA) using an audio mixing
   desk and wireless microphone receivers.
#. Room Coordinator or a Video Team member assisting/learning.

Beyond the talk rooms, the video team also needs additional spaces for:

* Build-up configuration and testing, before DebConf starts.
* Storing equipment before DebConf starts, and spare equipment during
  the event.
* Monitoring live streams during the event, and debugging issues.
  This requires desks, chairs, and ideally computer monitors.
* Network and server equipment.

Server equipment can be kept in a venue's own server room, if
after-hours access can be provided to the core video team members.
Or, if sufficient network connectivity, power, and cooling can be
provided, this can all be hosted in a single (lockable) room.

Provided equipment
------------------

Often the chosen venue has some audio/visual equipment available for use to use
in the talk rooms. What is available needs to be determined early on so that the
hire list can be adjusted as needed.

The most important piece of equipment usually provided by the venue is one (or
more) projector(s) in each talk room. These need to be able to take HDMI input,
as this is what the Opsis outputs. They should also be of sufficient quality so
that the image projected is clearly visible. If there are any switchers or range
extenders between the computer input and the projector, these should also be
identified.

The next useful pieces of equipment are speakers. We can hire these, but it is
easier if the venue has a room PA that we can connect into. We require stereo
(left and right) 3 pin `XLR`_ or balanced `1/4" jack`_ line inputs that we can
connect our mixer into.

.. _`XLR`: https://en.wikipedia.org/wiki/XLR_connector
.. _`1/4" jack`: https://en.wikipedia.org/wiki/Phone_connector_(audio)

Computer equipment
------------------

As we use a free-software stack, a lot of our video mixing and streaming
is done on computers running Debian, rather than professional video
production hardware.
The video team only has enough equipment for single-track miniconfs, for
a larger event like DebConf, we need to borrow / hire additional
equipment.
Some of these need to be desktop-style PCs with free PCI-e slots that we
can put video capture cards in.

This means we need quite a few computers to put on the event. For a
typical DebConfs with 3 rooms with video coverage, this requires:

* 4 x mid-range Desktop PC for video mixing.
  Modern CPU, >= 16GiB RAM, >= 1TiB storage, 2x free PCI-E sockets.
* 1 x storage server, with >=10TiB of storage.
  Preferably with 10Gbe network connectivity.
* 1 x network router / DHCP server.
  Preferably multiple network ports, or 10Gbe + VLANs.
* 1 x video stream encoder node, >= mid-range Desktop CPU.
* 3 x video publishing encoder nodes, >= mid-range Desktop PCs.
* 3 x basic laptop for presenters who have trouble with their own
  machines.

If the storage server is sufficiently modern with high-end CPUs and lots
of RAM, the encoder nodes aren't necessary.
But having a few extra computers is always useful.

We need to be able to open machines to install PCI-E cards, and
re-install operating systems on them.

If possible, keeping the storage server available online for a couple of
weeks after DebConf allows the team to resolve any issues discovered in
the published videos, and re-publish them.

Network Requirements
--------------------

Generally the video team needs a Gigabit network link from the podium of
each room, to the video mixing desk at the back of the room.
And a Gigabit link from the mixing desk to the server room.
Either these can be separate drops to the same switch, or cables can be
run in the room to a switch at the mixing desk.

Usually the video team runs its own segregated flat network, either on
dedicated cables or a VLAN on the venue network.
This is routed to the Internet through the video team's router.

Ideally, there'd be 10Gbps between servers.

See :external+debconf:ref:`network-requirements`.

Checklist
---------

Now that you understand why we need these things, here is a checklist you can
use to provide us with the information we need:

- Floor plans (if possible)
- Detailed pictures of the rooms
- Detailed pictures of the existing audio and video equipment in the rooms
- Position of the power sockets
- Position and speed of the network sockets
- Network uplink speed
- Network and firewall topology
- Review of the local safety laws with regards to cable runs and large objects
  (tables, etc.) in walkways
- Plans on how to provide us with computers and servers
