.. _ansible:

Ansible
=======

We use Ansible to configure all of our machines. You don't *need* to be familiar
with Ansible to use our playbooks, but it surely helps, especially if you want
to customise your setup.

You can find our Ansible git repository `here`_. Each role is documented within
its own directory, with the high level `README`_ giving an explanation on how
to hack on the ansible setup.

If you have never used Ansible before, don't worry. The `documentation`_ is
pretty well made. You can also find a very basic guide on how to get started
with Ansible on this page.

Before trying to use our playbooks, we recommend you get to know our
:ref:`setup <room setup>` some more.
We classify machines during the install by their role and you will have
a hard time understanding what Ansible does if you don't have a basic
idea of what you are trying to build.

.. _`documentation`: https://docs.ansible.com/ansible/latest/intro.html
.. _`here`: https://salsa.debian.org/debconf-video-team/ansible
.. _`README`: https://salsa.debian.org/debconf-video-team/ansible/blob/main/README.md
.. _`setup`: room_setup.html

Using Our Playbooks
-------------------

A default configuration that supports one room, with three machines (gateway,
vocotomix and opsis) is described in our
:external+dcvansible:doc:`index`.
There is also documentation around using a separate configuration
repository for more complicated setups.
