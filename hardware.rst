.. _hardware:

Hardware
========

This is a list of the hardware we use. You do not need to replicate it perfectly,
but it might give you some inspiration.
Here you can see :ref:`Diagrams of typical rooms setups used by the
video team<room setup>`.

.. _cameras:

Video cameras
-------------

The cameras we use are `Sony PXW-X70`_. We decided to buy this model for its
performance in low-light due to its large sensor and the good zoom lens they
have. They are also small and lightweight, which is a plus for shipping them
around the globe.

We decided to buy the 4k firmware unlock they have to further future proof them.

If you want to use a different camera, you may use either:

* A camera with HDMI or SDI output (typically from a higher-end camera) via a
  SDI capture card.
* A webcam via USB.

Some other nice-to-have features of the camera include:

* The camera should be able to do 720p (we don't do 1080p because it would
  require other changes to the infrastructure).
* SDI output: SDI cables are easier to run over long distances compared
  to HDMI. (USB is much more limited here).
* 2 XLR inputs for capturing audio from mixer together with video and outputting
  them to live mixing Computer.
* The output needs to be clean (without the camera UI on it).
* It needs to run for a long time without overheating.

.. _Sony PXW-X70: https://pro.sony.com/bbsc/ssr/micro-xdcam/cat-broadcastcameras/product-PXWX70/

Computers for live mixing
-------------------------

Each room needs a computer for live mixing. We typically refer to this machine
as the `voctomix`_ PC, named after the live mixing software we are using.

With three inputs (two cameras and the presenter's screen capture), Voctomix
needs a fairly powerful CPU to run properly. We highly recommend using
machines with Intel i7 CPUs from 2015 or later.

Using less powerful CPUs results dropped frames and audio synchronisation
issues.

The live mixing machine includes the following hardware:

* 2 x `SDI capture card`_
* 1 x >= 4 port gigabit Ethernet switch
  One port for the voctomix PC, one for the uplink, one for the capture PC
  (speaker laptop output capture) and one spare for debugging purposes (or for
  the speaker, if wifi gets flaky) Having more than one spare port is useful.
* 2 x Serial adapters for tally-lights
* Assorted cabling

.. _voctomix: https://github.com/voc/voctomix
.. _SDI capture card: https://www.blackmagicdesign.com/products/decklink

Tally lights
------------

Tally lights are little LEDs that are taped on the cameras and light up when the
camera is the active camera being recorded. It gives feedback to the camera
operator and this greatly improves the quality of the recording.

We use serial to Ethernet adapters and run Ethernet cables along the SDI cables
to the cameras.

.. _audio hardware:

Audio
-----

We normally rent most of the audio gear we need for DebConfs, since we do not
have enough audio kits for three rooms.

A typical room setup will use:

* 2 x headset mic for presenters. Headset mics provide good quality sound, while
  still being easy to put on presenters and ensures a constant distance between
  the presenter's mouth and the mic
* 2 x handheld cordless mic for questions from audience
* 2 x ambient omni-directional condenser mics for room noise. These only get
  sent to the camera feed, as they allow any questions asked without a mic to be
  heard on camera
* 2 x mic stand for the ambient mics
* 1 x stereo DI Box for capturing the presenter laptop sound
* 1 x mixer (8 channels XLR in, aux and mains out). The aux out gets sent to the
  house speakers and the mains to the main camera for recording and streaming
* 1 x audio snake with 8 inputs and 4 returns (not all of those are used,
  this just describes a configuration that's actually available on the market)
  6 of the 8 inputs are used for mixer inputs and thus get connected to the mixing desk
  (4 for radio mic receivers, 2 for DI Box left/right),
  returns can be used for connection to speakers (instead of direct connection from mixing desk),
  if the speakers are placed at the front of the room far away from the mixer.
* 2 x speakers (active or with amplifier) for sound reinforcement in the venue
* Assorted cabling to connect the various pieces of equipment

.. _laptop_capture:

Laptop output capture
---------------------

To capture the output of the speaker's laptop, we use a Asus TUF Gaming Capture
Card (`CU4K30`_). The USB output goes to our capture PC, whereas one of
the HDMI outputs goes to the room's projector.

You will need:

* 1 x Asus TUF Gaming Capture Vard
* 1 x capture PC (we use small `Minnowboard Turbots`_)
* Assorted cabling

.. _`Minnowboard Turbots`: https://minnowboard.org/minnowboard-turbot/
.. _`CU4K30`: https://www.asus.com/uk/accessories/streaming-kits/video-capture-streaming/tuf-gaming-capture-box-cu4k30/
