.. _room setup:

Room setup
==========

This section describes the typical layout of a talk room at a DebConf. It
includes the `video`_ and `audio`_ components. An `online conference`_ has all
required functions remotely operated.

Video
-----

|room-setup-video|

The video hardware we use is described in the :ref:`video hardware
section <cameras>`.
There are two cameras in every room and an HDMI capture device for
capturing the presenter's presentation. These inputs are mixed together
using Voctomix.
The presentation output is streamed over the wired network by the capture
PC to Voctomix.
The output from Voctomix is streamed out to the stream servers and
recorded locally for upload to the review system.

.. |room-setup-video| image:: /_static/room_setup_video.svg

Audio
-----

|room-setup-audio|

The audio setup described here uses the equipment listed in the
:ref:`hardware section <audio hardware>`.
It uses:

* two headset mics for presenters
* two handheld mics for questions
* two omni-directional mics for ambient noise
* one stereo DI for the presenter's laptop audio

Ambient noise is the clapping and general buzz that happens in the background
during a talk. This is useful for the stream, as it makes viewers feel as though
they are in the room. These two mics are not sent to the room audio system. The
presenter's laptop audio is captured to ensure that any audio or video played
during the talk can also be played on the stream and PA.

There are two audio mixes which need to be somewhat separate - one for the PA
and one for the stream. Most desks can support this using an aux bus or
sub-groups. Using aux buses requires 2 post-fader buses (left and right). This
is so the channel fader affects both the PA and the stream levels. The main-mix,
or one of the sub-groups, is run to the camera for the stream and recording.
This way, the camera handles synchronizing the audio and video. The aux bus or
other sub-group pair is run to the room's speakers.

.. |room-setup-audio| image:: /_static/room_setup_audio.svg

Combined
--------

Combined, this looks like this:

|room-setup|

.. |room-setup| image:: /_static/room_setup.svg

Online Conference
-----------------

|online-setup|

Voctomix is controlled via a web interface by the Director. It mixes between the
various sources that are streamed out to the audience. The Talkmeister and
presenters communicate in a Jitsi call. This is streamed through Voctomix for
the Q&A section of pre-recorded talks and the full talk for BoFs and live
events.

* Recordings are uploaded to sReview for transcoding
* Recordings are then played back through vogol
* Jitsi is used for Q&A and BoFs streaming rooms
* Jibri is used to record Jitsi streams. We have two instances to have a fallback
* EtherpadLite is used for people to ask questions during Q&A and for taking notes during BoFs
* Voctomix is the live video mixing software we use
* Vogol is a Voctomix front end
* We use RTMP to stream at low latency
* We also use HLS to stream, as it has wider compatibility even though it comes with higher latency

.. |online-setup| image:: /_static/online_setup.svg
